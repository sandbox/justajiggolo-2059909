<?php

/**
 * This function 'includes' several files with conflicting function names.
 * It must therefore be called from an external file and reloaded for each
 * version.
 * @param string $version
 */

//@todo: make this debuggable via the soap interface
function _generate_lists($version = '11.0') {
  //get available versions
  $versions = _available_versions();
  //return if the requested version is not available
  if(!isset($versions[$version])) {
    return NULL;
  }
  
//   $readfile = $versions[$version];
  $writefile = __DIR__ . "/qb_lists.inc";
//   module_include($readfile);
  $file_header = "<?php
      
/**
 * @file Quickbooks list reference for QBXML versions:
 * " . implode("\n * ", array_keys($versions)) . "
 * 
 * This file was automatically generated by qb_list_gen.php. Please not do edit
 * this file manually. Hopefully this file came with a readme.txt for more
 * information.
 *
 * This file is intended to privide a 'cached' list of available QB static 
 * list items.
 */\n\n";
  
  $out_array = array();
  $temp = drupal_get_path('module', 'qb_static_lists') . '/temp';
  if(!is_dir($temp)) {
    mkdir($temp);
  }
  foreach($versions as $ver => $filename) {
    //copy each file to local dir
//     copy($filename, $temp . "/qbxml.{$ver}.inc");
    //edit functions to reflect version
    $verstr = preg_replace('/([\d]{1,2})\.(\d*)/', '\1_\2', $ver);
    //must change BOTH function names
    $re1 = '/(function _qb_qbxml_types)(\(\)) \{(\n){0,1}/';
    $re2 = '/(function _qb_qbxml_queries)(\(\)) \{(\n){0,1}/';
    $in_file = file_get_contents($filename);
    $out_file = $temp . "/qbxml.{$ver}.inc";
    //find $re in $in_file, send results to $out_file
    $out_contents1 = preg_replace($re1, 
        "function _qb_qbxml_types_{$verstr}() {\n", 
        $in_file);
    $out_contents2 = preg_replace($re2, 
        "function _qb_qbxml_queries_{$verstr}() {\n", 
        $out_contents1);
    file_put_contents($out_file, $out_contents2);
    
    //read array from each file/function
    require_once $out_file;
    $func = "_qb_qbxml_queries_{$verstr}";
    if(function_exists($func)) {
      $out_array[$ver] = $func();
    }
    //delete files fo rmdir works
    unlink($out_file);
  }
  rmdir($temp);
  
  //filter list to only contain the static lists
  _qb_static_lists_filter_list($out_array);
  
  $output = $file_header . "\nfunction qb_types(\$version = $version, \$list = NULL) {\n";
  $output .= "  \$list_items = " . var_export($out_array, TRUE) . ";\n";
  $output .= "  if(!isset(\$version)) {\n";
  $output .= "    \$version = $version;\n";
  $output .= "  }\n";
  $output .= "  if(isset(\$list)\n";
  $output .= "     && isset(\$list_items[\$version])\n";
  $output .= "     && isset(\$list_items[\$version][\$list])) {\n";
  $output .= "    return = \$list_items[\$version][\$list];\n";
  $output .= "  }\n";
  $output .= "  elseif(isset(\$list_items[\$version]) {\n";
  $output .= "    return = \$list_items[\$version];\n";
  $output .= "  }\n";
  $output .= "  else {\n";
  $output .= "    return = \$list_items;\n";
  $output .= "  }\n";
  $output .= "}\n";
  
  file_put_contents($writefile, $output);
}

function _available_versions($folder = NULL) {
  if(!isset($folder)) {
    $folder = drupal_get_path('module', 'qb') . '/includes/qbxml/';
  }
  
  $versions = array();
  $folder = drupal_get_path('module', 'qb') . '/includes/qbxml/';
  foreach(scandir($folder) as $index => $filename) {
    $re = "/qbxml\.([\d]{1,2}\.\d*)\.inc/"; //@todo: should word boundaries be used?
    $val = array();
    if(preg_match($re, $filename, $val)) {
      $versions[$val[1]] = $folder . $filename;
    }
  }
  ksort($versions);
  return $versions;
}

function _qb_static_lists_filter_list(&$list) {
  foreach($list as $ver => $items) {
    $list[$ver] = _qb_static_lists_filter_list_r($items);
  }
}

function _qb_static_lists_filter_list_r($list) {
  $filtered = array();
  foreach($list as $key => $value) {
//     //sometimes, there is a '' key, so shift up, and rename appropriately
//     if(isset($value[''])) {
//       $k = $key . 'choice';
//       unset($list[$key]);
//       $list[$k] = $value;
//     }
//     else {
//       $k = $key;
//     }
    
    if(is_array($value)) {
      if(isset($value['type']) 
          && $value['type'] == 'ENUMTYPE'
          && isset($value['options'])
          && !isset($filtered[$key])) {
        //we are looking for this, add to filtered if not already there
        if (isset($value['options'])) {
          $filtered[$key] = $value;
          }
        }
        elseif(isset($value[''])
            && is_array($value[''])
            && isset($value['']['choice'])
            && !isset($filtered[$key . 'Choice'])) {
          //this is another potential list to be used in queries
          $filtered[$key . 'Choice']['options'] = $value['']['choice'];
        }
        else {
          //there might be something deeper
          $deeper = _qb_static_lists_filter_list_r($value);
          $filtered = array_merge($filtered, $deeper);
        }
      }
    else {
      //discard
    }
  }
  return $filtered;
}

